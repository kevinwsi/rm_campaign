<?php
	# connection setting
	switch( filter_input(INPUT_SERVER, 'HTTP_HOST') ) {
		case 'localhost':
			$_CONFIG = array(
				'username' => 'root',
				'password' => '',
				'database' => 'rmdb',
				'site_url' => '/rm_campaign/',
				'base_url' => '/rm_campaign/assets/',
				'plugins' => '/rm_campaign/plugins/'
			);
			break;
		default:
			$_CONFIG = array(
				'username' => 'rmdba',
				'password' => 'RM4dm1n@2016',
				'database' => 'rmdb',
				'site_url' => '/erm/promo/',
				'base_url' => '/erm/promo/assets/',
				'plugins' => '/erm/promo/plugins/'
			);
	}
	
	define('HOSTNAME', 'localhost');
	define('USERNAME', $_CONFIG['username']);
	define('PASSWORD', $_CONFIG['password']);
	define('DATABASE', $_CONFIG['database']);
	
	# open connection to server
	mysql_connect(HOSTNAME, USERNAME, PASSWORD) or die('Connection couldn\'t be established.');
	mysql_select_db(DATABASE) or die('Database not found.');
	
	# set the url for accessing link
	define('SITE_URL', $_CONFIG['site_url']);
	define('BASE_URL', $_CONFIG['base_url']);
	define('PLUGINS', $_CONFIG['plugins']);

	# set date and time
	date_default_timezone_set("Asia/Jakarta");
	define('SYS_DATE', date('Y-m-d H:i:s'));
	
	# set table prefix
	define('RESPONDERS','idc_users');
