$(document).ready(function(){   
        $('#register-box').hide();
        //show the canvas to get width then hide it again
        $('#game').show(); 
        var CANVAS_WIDTH = $('#game').width();
        $('#game').hide(); 
        var CANVAS_HEIGHT = 600;
        var FPS = 60;
        var lives = 5;
        var score = 0;
        var enemies_killed = 0;
        //var spawn_rate = 0.02;
        var spawn_rate = 0.04;
        //var bomb_spawn_rate = 0.002;
        var bomb_spawn_rate = 0.003;
        var spawn_counter = 0;
        var enemy_speed = 1;
        var level = 1;
        var playerBullets = [];
        var enemies = [];
        var bombs = [];
        var timer = 0;


        
        var canvasElement = $("<canvas id='game_canvas' width='" + CANVAS_WIDTH + 
                            "' height='" + CANVAS_HEIGHT + "'style='display:block; padding:0px; margin:0 auto; background-image:url(assets/bg_game1.png);'></canvas>");
        var canvas = canvasElement.get(0).getContext("2d");
        canvasElement.appendTo('#game');

        var elem = document.getElementById('game_canvas');    
        
        






        function draw() {  
            canvas.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
            //player.draw();    
            //playerBullets.forEach(function(bullet) {
                //bullet.draw();
            //});
            enemies.forEach(function(enemy) {
                enemy.draw();
            }); 
            bombs.forEach(function(bomb) {
                bomb.draw();
            }); 
        }         
        function display_score()
        {
            var c=document.getElementById("game_canvas");
            var ctx=c.getContext("2d");

            ctx.font="25px Comic Sans MS";
            ctx.fillText('Skor: '+score+' pts',10,110);

            // Create gradient
            //var gradient=ctx.createLinearGradient(0,0,c.width,0);
            //gradient.addColorStop("1.0","red");
            // Fill with gradient
            //ctx.fillStyle=gradient;
        }
        function display_difficulty()
        {
            var c=document.getElementById("game_canvas");
            var ctx=c.getContext("2d");

            ctx.font="25px Comic Sans MS";
            ctx.fillText('Level: '+level,10,150);

            // Create gradient
            //var gradient=ctx.createLinearGradient(0,0,c.width,0);
            //gradient.addColorStop("1.0","red");
            // Fill with gradient
            //ctx.fillStyle=gradient;
        }   
        function display_lives()
        {
            var c=document.getElementById("game_canvas");
            var ctx=c.getContext("2d");

            ctx.font="25px Comic Sans MS";
            ctx.fillText('Nyawa: '+lives,10,70);

            // Create gradient
            //var gradient=ctx.createLinearGradient(0,0,c.width,0);
            //gradient.addColorStop("1.0","red");
            // Fill with gradient
            //ctx.fillStyle=gradient;
        }   
        
        function display_timer()
        {
            var c=document.getElementById("game_canvas");
            var ctx=c.getContext("2d");

            ctx.font="25px Comic Sans MS";
            ctx.fillText('Waktu: '+Math.round((1000-timer)/100),10,30);

            // Create gradient
            //var gradient=ctx.createLinearGradient(0,0,c.width,0);
            //gradient.addColorStop("1.0","red");
            // Fill with gradient
            //ctx.fillStyle=gradient;
        } 
        var player = {
            color: "#00A",              
            width: 32, 
            height: 32,
            x: ($('#game').width()/2)-16,
            y: ($('#game').height()/2)-16,              
            draw: function() {
                canvas.fillStyle = this.color;
                canvas.fillRect(this.x, this.y, this.width, this.height);
            }
        };     

        function update() {
            if (keydown.left) {
                player.x -= 5;
            }

            if (keydown.right) {
                player.x += 5;
            }     

            if (keydown.space) {
                player.shoot();
            }
            playerBullets.forEach(function(bullet) {
                bullet.update();
             });

            playerBullets = playerBullets.filter(function(bullet) {
                return bullet.active;
            });                
            enemies.forEach(function(enemy) {
              enemy.update();
            });

            enemies = enemies.filter(function(enemy) {
              return enemy.active;
            });

            bombs.forEach(function(bomb) {
              bomb.update();
            });

            bombs = bombs.filter(function(bomb) {
              return bomb.active;
            });
            if(Math.random() < spawn_rate) {
              enemies.push(Enemy());          
            }    
            if(Math.random() < bomb_spawn_rate && level>=2) {
              bombs.push(Bomb());
            }  
            player.x = player.x.clamp(0, CANVAS_WIDTH - player.width);
            $('#score').html(score);
            $('#enemy_speed').html(enemy_speed);
        }


        function Bullet(I) {
            I.active = true;

            I.xVelocity = 0;
            I.yVelocity = -I.speed;
            I.width = 3;
            I.height = 3;
            I.color = "#000";

            I.inBounds = function() {
              return I.x >= 0 && I.x <= CANVAS_WIDTH &&
                I.y >= 0 && I.y <= CANVAS_HEIGHT;
            };

            I.draw = function() {
              canvas.fillStyle = this.color;
              canvas.fillRect(this.x, this.y, this.width, this.height);
            };

            I.update = function() {
              I.x += I.xVelocity;
              I.y += I.yVelocity;

              I.active = I.active && I.inBounds();
            };

            return I;
        }   

        player.shoot = function() {
            var bulletPosition = this.midpoint();

            playerBullets.push(Bullet({
              speed: 5,
              x: bulletPosition.x,
              y: bulletPosition.y
            }));
        };

        player.midpoint = function() {
            return {
              x: this.x + this.width/2,
              y: this.y + this.height/2
            };
        };            

        elem.addEventListener('click', function(e) {
                //console.log('click: ' + e.offsetX + '/' + e.offsetY); 
                var rect = collides(enemies, e.offsetX, e.offsetY);
                var bomb_rect = bomb_collides(bombs, e.offsetX, e.offsetY);
                if (rect) {
                    var counter = rect;
                    //enemies[counter].explode();
                    canvas.clearRect(enemies[counter].x, enemies[counter].y, enemies[counter].width, enemies[counter].height);
                    score +=level;
                    enemies_killed +=1;
                    //animate explosion   
                    //enemies.isExplode = true;
                    enemies[counter].explode();
                    if(enemies_killed===level*5)
                    {
                        enemies_killed = 0;
                        enemy_speed+=0.4;
                        spawn_rate+=0.002;
                        bomb_spawn_rate+=0.0001;
                        level+=1;
                    }
                    //setInterval(function() {
                    //delete enemies[counter];                
                    //}, 2000);                
                } 
                if (bomb_rect) {
                    var bomb_counter = bomb_rect;
                    //bombs[bomb_rect].explode();
                    canvas.clearRect(bombs[bomb_rect].x, bombs[bomb_rect].y, bombs[bomb_rect].width, bombs[bomb_rect].height);
                    score -=level*5;
                    lives -=5;
                    if(score<=0)
                    {
                        score=0;
                    }
                    bombs[bomb_rect].explode();
                    //setInterval(function() {
                    //delete bombs[bomb_rect];                
                    //}, 2000);                
                }             
            }, false); 

        function collides(enemies, x, y) {
            var isCollision = false;
            for (var i = 0, len = enemies.length; i < len; i++) {
                var left = enemies[i].x, right = enemies[i].x+enemies[i].width;
                var top = enemies[i].y, bottom = enemies[i].y+enemies[i].height;
                if (right >= x-10
                    && left <= x+10
                    && bottom >= y-20
                    && top <= y+20) {
                    isCollision = [i];
                }
            }
            return isCollision;                       
        }  
        function bomb_collides(bombs, x, y) {
            var isCollision = false;
            for (var i = 0, len = bombs.length; i < len; i++) {
                var left = bombs[i].x, right = bombs[i].x+bombs[i].width;
                var top = bombs[i].y, bottom = bombs[i].y+bombs[i].height;
                if (right >= x-10
                    && left <= x+10
                    && bottom >= y-20
                    && top <= y+20) {
                    isCollision = [i];
                }
            }
            return isCollision;                       
        }


        function Enemy(I) {
          I = I || {};

          I.active = true;
          I.age = Math.floor(Math.random() * 128);

          I.color = "#A2B";
          I.x = CANVAS_WIDTH/8 + Math.random() * CANVAS_WIDTH/1.25;
          I.y = 0;
          I.xVelocity = 0
          I.yVelocity = enemy_speed;
          I.width = 45;
          I.height = 45;
          I.count_frame = 0;
          I.inBounds = function() {
                return I.x >= 0 && I.x <= CANVAS_WIDTH &&
                I.y >= 0 && I.y <= CANVAS_HEIGHT;           
          };

          I.CheckHeight = function() {
                return I.y >= 0 && I.y <= CANVAS_HEIGHT;
          };

          I.sprite = Sprite("kerupukoutline.png");
          I.sprite_explode = Sprite("b1.png");
          I.sprite0 = Sprite("kerupuk_0.png");
          I.sprite1 = Sprite("kerupuk_1.png");
          I.sprite2 = Sprite("kerupuk_2.png");
          I.sprite3 = Sprite("kerupuk_3.png");
          I.sprite4 = Sprite("kerupuk_4.png");
          I.sprite5 = Sprite("kerupuk_5.png");
          I.sprite6 = Sprite("kerupuk_6.png");
          I.sprite7 = Sprite("kerupuk_7.png");
          I.sprite8 = Sprite("kerupuk_8.png");


          I.draw = function() {       
            //canvas.fillStyle = this.color;
            //canvas.fillRect(this.x, this.y, this.width, this.height);
            if(I.yVelocity === 0)
            {           
                this.sprite_explode.draw(canvas, this.x, this.y)
                setInterval(function() {
                    I.active=false;
                }, 1200);

                //I.active=false;
            }
            else
            {
                this.sprite.draw(canvas, this.x, this.y);
            }

          };

          I.explode = function() {
            //console.log(this.test);
            //canvas.fillStyle = this.color;
            //canvas.fillRect(this.x, this.y, this.width, this.height);
            //var animation_frame = 0;
            //do{
                //var current_animation_frame = animation_frame%9;
                //I.sprite_explode = Sprite("kerupuk_"+current_animation_frame+".png");                       
                //this.sprite_explode.draw(canvas, this.x, this.y);
                //animation_frame++;
                //console.log(current_animation_frame);
            //}while(animation_frame<=25);
            I.yVelocity = 0;
            //I.active = false;
          };

          I.update = function() {
            I.x += I.xVelocity;
            I.y += I.yVelocity;
            I.xVelocity = 0;
            //I.xVelocity = 3 * Math.sin(I.age * Math.PI / 64);
            I.age++;
            I.active = I.active && I.inBounds();
            if(!I.CheckHeight())
            {
                lives -=1;
                if(score<=0)
                {
                    score=0;
                }
                //console.log(score);
            }
          };

          return I;
        };

        function Bomb(I) {
          I = I || {};

          I.active = true;
          I.age = Math.floor(Math.random() * 128);

          I.color = "#A2B";
          I.x = CANVAS_WIDTH/8 + Math.random() * CANVAS_WIDTH/1.25;
          I.y = 0;
          I.xVelocity = 0
          I.yVelocity = enemy_speed;
          I.width = 23;
          I.height = 40;

          I.inBounds = function() {
                return I.x >= 0 && I.x <= CANVAS_WIDTH &&
                I.y >= 0 && I.y <= CANVAS_HEIGHT;           
          };

          I.CheckHeight = function() {
                return I.y >= 0 && I.y <= CANVAS_HEIGHT;
          };
          I.sprite = Sprite("petasan_boom.png");
          I.sprite_explode = Sprite("b2.png");

          I.draw = function() {
            //canvas.fillStyle = this.color;
            //canvas.fillRect(this.x, this.y, this.width, this.height);

            if(I.yVelocity === 0)
            {           
                this.sprite_explode.draw(canvas, this.x, this.y)
                setInterval(function() {
                    I.active=false;
                }, 1200);

                //I.active=false;
            }
            else
            {
                this.sprite.draw(canvas, this.x, this.y);
            }
          };

          I.explode = function() {
            //canvas.fillStyle = this.color;
            //canvas.fillRect(this.x, this.y, this.width, this.height);
            I.yVelocity = 0;
            //this.sprite_explode.draw(canvas, this.x, this.y);
          };

          I.update = function() {
            I.x += I.xVelocity;
            I.y += I.yVelocity;
            I.xVelocity = 0;
            //I.xVelocity = 3 * Math.sin(I.age * Math.PI / 64);
            I.age++;
            I.active = I.active && I.inBounds();
            if(!I.CheckHeight())
            {
                if(lives<50)
                {
                    lives +=1;
                }            
                console.log(score);
            }
          };

          return I;
        };

    $("#game_start").click(function(){
        $('#game').show();
        var game_run_time = setInterval(function() {
                                update();
                                draw();
                                display_score();
                                display_lives();
                                //display_difficulty();
                                display_timer();
                                timer++;
                                console.log(timer);
                                if (lives<=0 || timer>=1000)
                                {
                                    canvas.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
                                    $('#end_game_score_menu').show();       
                                    $('#end_game_score').html(score);  
                                    $('#game_score').val(score); 
                                    clearInterval(game_run_time);
                                    $('#game').remove();
                                    $('#register-box').show();
                                }                        
                            }, 1000/FPS);
        $('#game_start_menu').remove();
    });
    $("#game_end").click(function(){
        $('#game').hide();    
    });
    
});
