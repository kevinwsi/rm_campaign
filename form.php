<?php
    session_start();
    include 'config/connection.php';
    include 'helpers/helper.php';
    require_once 'plugins/facebook-sdk-v5/autoload.php';
    include_once("plugins/twitter/config.php");		
    include_once("plugins/twitter/inc/twitteroauth.php");
    
    # params
	$id_no = '';
	$date_of_birth = '';
    $gender = '';
	$phone = '';
    $email = '';
    $address = '';
	$default_area = '';
	$add_area = '';
	$voucher = '';
	
    $error_count = 0;
	$id_no_error = '';
    $email_error = '';
    $phone_error = '';
    $register = array(
		'date_add' => SYS_DATE,
		'status' => 1
	);     

	// for name
	if( isset($_SESSION['facebook']) ){
		$name = $_SESSION['facebook']['name'];
		$fbid = $_SESSION['facebook']['fbid'];
	} elseif( isset($_SESSION['twitter']) ){
		$name = $_SESSION['twitter']['user_data']->name;
		$tw = $_SESSION['twitter']['user_data']->screen_name;
	} else {
		$name = '';
		$fbid = '';
		$tw = '';
	}

	if( isset($_POST['game_score']) ) {
		$_SESSION['game_score'] = $_POST['game_score'];
	}// else {
	//	$_SESSION['game_score'] = '';
	//}

    # process are here
    if( isset($_POST['btn-submit']) ) {
        unset($_POST['btn-submit']);
		
        foreach($_POST as $key=>$val) {
            switch( $key ) {
				case 'id_no':
                    # check into database
                    $id_no = $val;
                    if( !empty($val) ) {
                    	$val = strtolower($val);
                        $chk_id = mysql_query("SELECT id_no FROM ".RESPONDERS." WHERE id_no = '".$val."'") or die(mysql_error());
                        $db_id = mysql_fetch_assoc($chk_id);
                        if( $db_id['id_no'] == $val ) {
                            $id_no_error = '<span class="text-maroon">Email sudah digunakan</span>';
                           	$error_count++;
                        }
                     }
					 break;
                case 'name':
                    $val = ucwords(strtolower(str_replace(array('.','  '),array('',' '),$val)));
                    $name = $val;
                    break;
				case 'date_of_birth': $date_of_birth = $val; break;
                case 'sex': $gender = $val; break;
                case 'email':
                    # check into database
                    $email = $val;
                    if( !empty($val) ) {
                    	$val = strtolower($val);
                        $chk_email = mysql_query("SELECT email FROM ".RESPONDERS." WHERE email = '".$val."'") or die(mysql_error());
                        $db_email = mysql_fetch_assoc($chk_email);
                        if( $db_email['email'] == $val ) {
                            $email_error = '<span class="text-maroon">Email sudah digunakan</span>';
                           	$error_count++;
                        }
                     }
                    break;
                case 'phone': $phone = $val; break;
//                case 'phone':
//                    # check into database
//                    if( !empty($val) ) {
//                        $chk_phone = mysql_query("SELECT phone FROM ".RESPONDERS." WHERE phone = '".$val."' AND survey_type = 'sejutarumah'") or die(mysql_error());
//                        $db_phone = mysql_fetch_assoc($chk_phone);
//                        if( $db_phone['phone'] == $val ) {
//                            $phone_error = '<span class="text-maroon">No. Telepon/HP sudah digunakan</span>';
//                            $error_count++;
//                        }
//                    }
//                    $phone = $val;
//                    break;
                case 'address':
                    $val = str_replace(array('Rt','Rw'),array('RT','RW'),ucwords(strtolower($val)));
                    $address = $val;
                    break;
                case 'voucher_id': $voucher = $val; break;
                case 'default_area': $default_area = $val; break;
                case 'fb_id': $fbid = $val; break;
                case 'tw_screen_name': $tw = $val; break;
                case 'add_area':
                    $val = ucfirst(strtolower(mysql_real_escape_string($val)));
					$add_area = $val;
                    break;
            }            
            
            $register[$key] = $val;
		}
		//print_r($register); exit;

        # if no error found
//		echo $error_count; exit;
        if( $error_count == 0 ) {
			$register['password'] = auto_code('password', 8);
       		$post_data = formatting_query($register, ',');
            $qry_save = "INSERT INTO ".RESPONDERS." SET ".$post_data;
            $sql_save = mysql_query($qry_save) or die(mysql_error());
            
            if( $sql_save ) {
				// save data to rmdb
				$exp_name = explode(' ', $name);
				$count_name = str_word_count($name);
				$rmdb['fname'] = implode('', array_slice( $exp_name, 0, 1));
				$rmdb['sname'] = trim(implode(' ', array_slice( $exp_name, 1)));
				$rmdb['email'] = $email;
				$rmdb['contact_email'] = $email;
				$rmdb['phone'] = $phone;
				$rmdb['date_created'] = $register['date_add'];
				$rmdb['date_modified'] = $register['date_add'];
				$rmdb['user_type'] = 'private';
				$rmdb['status'] = 0;
				$rmdb['confirm'] = 0;
				$rmdb['confirm_code'] = auto_code('confirm_code', 10);
				
				// password, need to be encrypted
				$salt = 'rumahmurah';
				$start = substr($register['password'],0,3);
				$end = substr($register['password'],3);
				$rmdb['password'] = hash('SHA256', $start.$salt.$end);
				
				// social media account
				if( isset($_SESSIOO['facebook']) ) {
					$rmdb['fbid'] = $_SESSION['facebook']['fbid'];
				}
				if( isset($_SESSIOO['twitter']) ) {
					$rmdb['screen_name'] = $_SESSION['twitter']['user_data']->screen_name;
				}				
				//print_r($rmdb); exit;
				
				// validate email before save into rmdb
				$cek_rm_email = mysql_query("SELECT email, contact_email FROM rm_users WHERE email = '".$email."' OR contact_email = '".$email."'") or die(mysql_error());
				$rm_email = mysql_fetch_assoc($cek_rm_email);
				if( $rm_email['email'] != $email && $rm_email['contact_email'] != $email ) {
					//echo 'email belum ada yang pake'; exit;
					$rmdb_data = formatting_query($rmdb, ',');
					$qry_save_rmdb = "INSERT INTO rm_users SET ".$rmdb_data;
					$sql_save_rmdb = mysql_query($qry_save_rmdb) or die(mysql_error());
				} else {
					$sql_save_rmdb = false;
				}

				if( $sql_save_rmdb == true ) {
					// redirect
					header('location:'.$_SERVER['PHP_SELF'].'?add=success');
				}
            }
        }
    }
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php if( empty($_SESSION['game_score']) ) { ?>
        <meta property="og:url" content="http://rumahmurah.com/erm/promo/form.php" />
        <meta property="og:title" content="Rumahmurah.com bagi-bagi voucher" />
    <?php } else { ?>
        <meta property="og:url" content="http://rumahmurah.com/promo" />
        <meta property="og:title" content="Skor Merdekaku <?php echo $_SESSION['game_score']; ?> | Bisa dapat voucher hanya di rumahmurah.com" />
    <?php } ?>
    <meta property="og:description" content="Dapatkan voucher promo hanya di rumahmurah.com tinggal isi formnya! Like and share @rumahmurahcom" />
    
    <title>Voucher promo rumah murah</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="<?php echo BASE_URL.'bootstrap/css/bootstrap.min.css'; ?>">
    <link rel="stylesheet" href="<?php echo PLUGINS.'font-awesome/css/font-awesome.min.css'; ?>">
    <link rel="stylesheet" href="<?php echo PLUGINS.'ionicons/css/ionicons.min.css'; ?>">
    <link rel="stylesheet" href="<?php echo PLUGINS.'iCheck/square/blue.css'; ?>">
    <link rel="stylesheet" href="<?php echo BASE_URL.'dist/css/AdminLTE.min.css'; ?>">

 
	<style type="text/css">
		body {
                    background-image: url('assets/img/bgcampaign.png');
                    background-repeat: no-repeat;
                    background-attachment: fixed;
                    background-position: center;
                    background-size: cover;
                }
	</style>
  </head>
  <!-- FB SCRIPT -->
  <div id="fb-root"></div>
  <script>
	(function(d, s, id) {
  		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
  		js = d.createElement(s); js.id = id;
  		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=1138147742908647";
  		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
  </script>
  <!-- END OF FB SCRIPT -->
  
<body class="hold-transition">
    <div style="padding-top:20px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12" align="center">
                    <img class="img-responsive" src="agen/img/head.png" width="400px;" /><br /> 
                    <div align="" class="col-md-3"></div>
                    <div class="col-md-6 clearfix" style="background-color:#fff">
                    	<?php if( isset($_GET['add']) && $_GET['add'] == 'success' ) { ?>
							<div class="alert alert-success alert-dismissable text-left" style="margin-top:15px;">
								<button type="button" class="close btn-close" data-dismiss="alert" aria-hidden="true">x</button>
								<i class="icon fa fa-check"></i> Terima kasih telah berpartisipasi.
							</div>
                        <?php } ?>
                        <div class="social-auth-links text-center">
                            <?php if(!isset($_SESSION['facebook'])) { ?>
                            	<a class="btn btn-block btn-social btn-facebook" href="plugins/facebook/fbconfig.php"> <i class="fa fa-facebook"></i> Hubungkan dengan Facebook</a>
                            <?php } ?>
                            <?php if(!isset($_SESSION['twitter'])) { ?>
                                <a class="btn btn-block btn-social btn-twitter" href="plugins/twitter/sign_in.php"> <i class="fa fa-twitter"></i> Hubungkan dengan Twitter</a>
							<?php }?>
                        </div>
                        <br>
                        <form id="regis-form" action="" method="post">
							<?php if( $error_count > 0 ) { ?>
                                <div class="alert alert-danger alert-dismissable text-left" style="margin-top:15px;">
                                    <button type="button" class="close btn-close" data-dismiss="alert" aria-hidden="true">x</button>
									<?php if( !empty($id_no_error) ) { ?>
										<i class="icon fa fa-close"></i> No. Identitas <b><?php echo $id_no; ?></b> sudah digunakan. <br>
                            		<?php } ?>
									<?php if( !empty($email_error) ) { ?>
                                    	<i class="icon fa fa-close"></i> Email <b><?php echo $email; ?></b> sudah digunakan.
                                    <?php } ?>
                                </div>
                            <?php } ?>
                            <div class="form-group clearfix">
                                <label class="row col-md-12 control-label" style="text-align:left">No. Identitas *</label>
                                <input type="text" class=" col-md-6 form-control" name="id_no" id="id_no" placeholder="KTP/SIM/Paspor" value="<?php echo $id_no; ?>" autofocus>
                                <div class="clearfix"></div>
                                <p class="help text-left"><i>No. identitas diperlukan untuk verifikasi data pemenang voucher</i></p>
                            </div>
                            <div class="form-group clearfix">
                                <label class="row col-md-12 control-label" style="text-align:left">Nama *</label>
                                <input class="form-control" name="name" id="name" type="text" placeholder="Nama" value="<?php echo trim($name); ?>">
                                <p class="help text-left clearfix"><i>Nama sesuai dengan identitas diri anda</i></p>
                            </div>
                            <div class="form-group clearfix">
                                <label class="row col-md-12 control-label" style="text-align:left">Tanggal Lahir</label>
                                <input class="form-control" name="date_of_birth" id="date_of_birth" type="text" placeholder="Tanggal lahir, contoh : 1993-05-14"  value="<?php echo $date_of_birth; ?>"/>
                            </div>
                            <div class="form-group clearfix">
                                <label class="row col-md-12 control-label" style="text-align:left">Jenis kelamin</label>
                                <div class="row">
                                    <div class="col-md-6 text-left">
                                    	<input class="sex" type="radio" name="sex" id="male" value="male" <?php if(!empty($gender) && $gender == 'male') echo 'checked'; else echo 'checked'; ?>> Laki-laki
                                   	</div>
                                    <div class="col-md-6 text-left">
                                    	<input class="sex" type="radio" name="sex" id="female" value="female" <?php if(!empty($gender) && $gender == 'female') echo 'checked'; ?>> Perempuan
                                    </div>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="row col-md-12 control-label" style="text-align:left">Email *</label>
                                <input class="form-control" name="email" id="email" type="email" placeholder="Email" value="<?php echo $email; ?>">
                            </div>
                            <div class="form-group clearfix">
                                <label class="row col-md-12 control-label" style="text-align:left">No. Telp/HP *</label>
                                <input class="form-control" type="text" name="phone" id="phone" placeholder="No. Telp/Hp" value="<?php echo $phone; ?>">
                                <p class="help text-left clearfix"><i>No. telp/hp yang aktif, diperlukan untuk menghubungi pemenang dan pengambilan hadiah</i></p>
                            </div>
                            <div class="form-group clearfix">
                                <label class="row col-md-12 control-label" style="text-align:left">Alamat</label>
                                <textarea rows="3" class="form-control" name="address" id="address" placeholder="Alamat"><?php echo $address; ?></textarea>
                            </div>
                            <div class="form-group clearfix">
                                <label class="row col-md-12 control-label" style="text-align:left">Hadiah</label>
                                <div class="row">
                                    <div class="col-md-6 text-left">
                                    	<input class="_v" type="radio" name="voucher_id" id="v_map" value="map" <?php if(!empty($voucher) && $voucher == 'map') echo 'checked'; else echo 'checked'; ?>>
                                        <img style="margin-left:20px;" src="agen/img/MAP.png" width="100px" />
                                    </div>
                                    <br>
                                    <div class="col-md-6 text-left">
                                    	<input class="_v" type="radio" name="voucher_id" id="v_cfr" value="carrefour" <?php if(!empty($voucher) && $voucher == 'carrefour') echo 'checked'; ?>>
                                        <img style="margin-left:20px;" src="agen/img/CR.png" width="100px" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group clearfix text-justify">
                                <b>Ayo..!!</b>
                                <p>Kamu ingin memiliki rumah, Kami mempunyai rumah dengan harga terjangkau, untuk di lokasi :</p>
                                <div class="row">
                                    <div class="col-md-4">
                                    	<input type="radio" class="_da" id="klari" name="default_area" name="default_area" value="Klari" <?php if(!empty($default_area) && $default_area == 'Klari') echo 'checked'; ?>> Klari, Karawang
                                    </div>
                                    <div class="col-md-4">
                                    	<input type="radio" class="_da" id="tambun" name="default_area" value="Tambun" <?php if(!empty($default_area) && $default_area == 'Tambun') echo 'checked'; ?>> Tambun, Bekasi
                                    </div>
                                    <div class="col-md-4">
                                    	<input type="radio" class="_da" id="other" name="default_area" value="other" <?php if(!empty($default_area) && $default_area == 'other') echo 'checked'; ?>> Lokasi Lain
                                    </div>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                            	<?php
									switch($default_area) {
										case 'other': $display = 'block'; break;
										default: $display = 'none'; $add_area = ''; break;
									}
								?>
                                <input class="form-control" name="add_area" id="add_area_field" type="text" placeholder="Tuliskan Lokasi yang kamu inginkan disini.." value="<?php echo $add_area; ?>" style="display:<?php echo $display; ?>;"/>
                            </div>
                            <div class="form-group clearfix" align="right">
                            	<br>
                                <button class="btn btn-danger" type="submit" name="btn-submit" id="btn-submit">Submit</button>
								<?php if( isset($_SESSION['twitter']) ) { ?>
                                  <input type="hidden" name="tw_screen_name" value="<?php echo $_SESSION['twitter']['user_data']->screen_name; ?>">
                                <?php } elseif( isset($_SESSION['facebook']) ) { ?>
                                  <input type="hidden" name="fb_id" value="<?php echo $_SESSION['facebook']['fbid']; ?>">
                                <?php } else { echo ''; } ?>

								<div class="fb-share-button pull-left" data-href="https://www.facebook.com/cicilrumahmurah" data-layout="button_count"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br /><br /><br />
    
    <!-- jQuery 2.1.4 -->
    <script src="<?php echo PLUGINS.'jQuery/jQuery-2.1.4.min.js'; ?>"></script>
    <script src="<?php echo PLUGINS.'iCheck/icheck.min.js'; ?>"></script>
    <script src="<?php echo BASE_URL.'bootstrap/js/bootstrap.min.js'; ?>"></script>
    <script src="<?php echo BASE_URL.'js/jquery.validate.js'; ?>"></script>
    <script src="<?php echo BASE_URL.'js/jquery.validate.additional.js'; ?>"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <script>
		// datepicker
		$( "#date_of_birth" ).datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'yy-mm-dd',
			yearRange: "1950:2016"
    	});
		
        // validate registration
        $("#regis-form").validate({
            rules: {
                id_no: {
                    required: true,
                    digits: true
                },
                name: {
                    required: true,
                    letterspaceonly: true
                },
				email: { email: true },
                phone: {
					required: true,
                    number: true,
                    minlength: 7
                },
				//voucher_id: 'required'
            },
            messages: {
                id_no: {
					required: "No. identitas wajib diisi",
					digits: "Gunakan angka"
				},
                name: {
					required: "Nama wajib diisi, sesuai identitas diri",
					letterspaceonly: "Gunakan huruf"
				},
				email: {
					email: "Email tidak valid"
				},
                phone: {
					required: "No. telp/hp wajib diisi",
                    number: "Hanya gunakan angka",
                    minlength: "Minimal 7 angka"
                },
				//voucher_id: "Pilih jenis voucher yang kamu inginkan"
            }
        });

		// For add another area
		$(document).on('click','._da',function(){
			var id = $(this).attr('id');
			
			if( id == 'other' ) {
				$('#add_area_field').show().focus();
			} else {
				$('#add_area_field').hide();
			}
		});
		
        // For Checkbox and Radio
        $('._v, .sex').iCheck({
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    </script>
</body>
</html>
