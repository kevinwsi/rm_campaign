<?php
    include 'config/connection.php';
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta property="og:url" content="http://rumahmurah.com/erm/promo" />
    <meta property="og:title" content="Voucher promo rumah murah" />
    <meta property="og:description" content="Ayo dapatkan voucher promo dari rumah murah hanya dengan mengisi form yang disediakan" />
    
    <title>Voucher promo rumah murah</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?php echo BASE_URL.'bootstrap/css/bootstrap.min.css'; ?>">
    <link rel="stylesheet" href="<?php echo PLUGINS.'font-awesome/css/font-awesome.min.css'; ?>">
    <link rel="stylesheet" href="<?php echo PLUGINS.'ionicons/css/ionicons.min.css'; ?>">
    <link rel="stylesheet" href="<?php echo PLUGINS.'iCheck/square/blue.css'; ?>">
    <link rel="stylesheet" href="<?php echo BASE_URL.'dist/css/AdminLTE.min.css'; ?>">

    <!-- game script -->
        <script language="javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script>
        <script src="js/jquery.hotkey.js" type="text/javascript"></script>
        <script src="js/key_status.js" type="text/javascript"></script>
        <script src="js/util.js" type="text/javascript"></script>
        <script src="js/game.js" type="text/javascript"></script>        
        <script src="js/sprite.js" type="text/javascript"></script>    
        <script src="https://code.createjs.com/createjs-2015.11.26.min.js"></script>    
	<style type="text/css">
		body {
                    background-image: url('assets/img/bgcampaign.png');
                    background-repeat: no-repeat;
                    background-attachment: fixed;
                    background-position: center;
                    background-size: cover;
                }
	</style>
    <!-- end of game script -->
  </head> 
  
<body> 
<div class='container' style='margin-bottom:20px;'>
<!-- game-->
    <!-- first prompt-->
    <div id="game_start_menu" class="col-md-12" style="top:5em;background-image:url(assets/img/bgw.png); background-repeat:no-repeat; background-size:100% 100%; background-position:center center; min-height:300px; font-family:'KG Corner of the Sky'; font-size:20px; margin:0 auto; text-align:center"> 
        <p style="padding:55px 25% 10px 25%; line-height:33px; color:#333;">Ayo ramaikan dirgahayu kita dengan main game lomba kerupuk di
        <font style="color:#C52433;">Rumah</font><font style="color:#F36F24">murah.com</font>,                
        Mau coba ??</p>
        <!--<p style="padding:68px 10px; line-height:33px; color:#fff;">Kamu mau dapat voucher belanja gratis ? </p>-->
        <a href="#" id="game_start" class="btn ya"><img src="assets/img/main.png" width="120px;" /></a>
        <a href="<?php echo SITE_URL.'form.php'; ?>" id="game_end" class="btn tidak"><img src="assets/img/tidak.png" width="120px" /></a>
    </div>
    <!-- end of first prompt -->
    <!--
    <div class='col-xs-6 col-sm-6 col-md-6 col-lg-6'  id="game_start" style="height:200px; background-color: blue;">Start Game</div>
    <div class='col-xs-6 col-sm-6 col-md-6 col-lg-6'  id="game_end" style="height:200px; background-color: purple;">End Game</div>
    -->
    <div class='col-xs-12 col-sm-12 col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2'  id="game" style='display:none; padding:0px; border:4px solid black;'>
    </div>
    <!-- score board -->
    <div id="end_game_score_menu" class='col-xs-12 col-sm-12 col-md-12 col-lg-12' style='height: 500px; text-align:center; display:none;'>
        <div class="col-md-3"></div>
        <div class="col-md-6" style="background-color:#E8E0B4; font-family:'KG Corner of the Sky'; font-size:20px; margin:0 auto; height:560px; border:10px solid #B4AE8B; border-radius:25px;">
                <div align="center"><img src="assets/img/pia1.png" width="170px;" /></div><br />
            <p class="text-center" style="font-family:'KG Corner of the Sky'; font-size:28pt; color:#DC332F">Selamat..!!</p>
            <p class="text-center" style="font-family:'KG Corner of the Sky'; font-size:13pt; color:red;">Skor anda</p>
            <p id="end_game_score" class="text-center" style="font-family:'KG Corner of the Sky'; font-size:38pt; color:blue;"></p><br />

            <form action="form.php" method="post">
                <input type="hidden" name="game_score" id="game_score">
                <input type="submit" <?php /*?>name="post_game_score" <?php */?>class="btn-info btn-lg" value="Lanjut ke form!">
            </form>
            <!--<a href="http://rumahmurah.com/erm/sejutarumah/form.php" id="game_end" class="btn tidak">Isi formulir</a>-->
        </div>
    </div>
    <!-- end of score board -->
<!-- game ends here -->  
</div>
</body>
</html>
